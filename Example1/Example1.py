import pandas as pd

if __name__ == "__main__":

    df = pd.read_csv('Book1.csv')                             # read the data in from excel/ csv
    df["followers_per_post"] = df["followers"] / df["posts"]  # create a new column with calculated data
    df.sort_values(by='followers_per_post', ascending=False)  # sort all rows in descending order of the selected column
    print(df)                                                 # print the dataframe in the console
